<?php
/**
 * Created by PhpStorm.
 * User: manonmoulin
 * Date: 20/01/15
 * Time: 21:00
 */

namespace Composers;


class BlogpostComposer {

    public function compose($view) {
        $view->with('blogpost', new \Post() );
    }
}