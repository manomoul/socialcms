<?php
namespace Composers;

class CompanyComposer {

	public function compose($view) {
		$view->with('company', \Company::find(1) );
	}

}
