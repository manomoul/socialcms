<?php namespace Composers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider {

	public function register() {
		$this->app->view->composer(array('layouts.partials.footer'), 'Composers\CompanyComposer');
		$this->app->view->composer(array('events.create'), 'Composers\EventComposer');
		$this->app->view->composer(array('images.create'), 'Composers\ImageComposer');
		$this->app->view->composer(array('posts.create'), 'Composers\BlogpostComposer');
		$this->app->view->composer(array('pages.create'), 'Composers\PageComposer');
	}

}