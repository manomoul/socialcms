<?php
/**
 * Created by PhpStorm.
 * User: manonmoulin
 * Date: 20/01/15
 * Time: 20:59
 */

namespace Composers;


class PageComposer {

    public function compose($view) {
        $view->with('page', new \Page() );
    }
}