<?php

class ApiController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getEvents($id = null)
    {
        if($id) {
            $respons = Agendaitem::find($id);
            return Response::json($respons, 200,
                array('Access-Control-Allow-Origin' => '*'));
        }

        $models = Agendaitem::all();

        return Response::json($models, 200,
            array('Access-Control-Allow-Origin' => '*'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getPages($slug = null)
    {
        if($slug) {
           // $respons = Page::find($id);
            $respons = Page::where('slug' , '=', $slug)->first();
            return Response::json($respons, 200,
                array('Access-Control-Allow-Origin' => '*'));
        }

        $models = Page::all();

        return Response::json($models, 200,
            array('Access-Control-Allow-Origin' => '*'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getBlogposts()
    {
        $models = Post::all();

        return Response::json($models, 200,
            array('Access-Control-Allow-Origin' => '*'));

    }

    /**
     * Display a listing of the resource.
     *url: /api/v1/config
     * @return Response
     */
    public function getConfig()
    {
        $model = Company::first();

        return Response::json($model, 200,
            array('Access-Control-Allow-Origin' => '*'));

    }

    /**
     * Display a listing of the resource.
     *url: /api/v1/config
     * @return Response
     */
    public function postMessage()
    {
        $input = Input::all();
        $validation = Validator::make($input, Page::$rules);
        echo(var_dump($input));
        if ($validation->passes()) {
            Page::create($input);
        }

    }
}
	