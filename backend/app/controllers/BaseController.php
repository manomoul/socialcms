<?php

/*use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;*/

class BaseController extends Controller
{

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected $layout = 'layouts.master';

    /*public function __construct() {
        FacebookSession::setDefaultApplication('872862016079923', '8dbc897ced0c1522b65933c40014bf23');
    }*/

    protected function setupLayout()
    {

        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }

    }

}
