<?php

class CompanyController extends \BaseController
{

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
        $model = Company::findorfail($id);
        $modelname = 'company';
        $this->layout->content = View::make('companies.show')->with(array('model' => $model, 'modelname' => $modelname));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $model = Company::findorfail($id);
        $modelname = 'company';
        $this->layout->content = View::make('companies.edit')->with(array('model' => $model, 'modelname' => $modelname));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
        $input = Input::except(array('_method', '_token'));
        /*$validation = Validator::make($input, Contact::$rules);
        if ($validation->passes()) {
            $model = Contact::find($id);
            $model->update($input);
            Session::flash('message', 'Successfully edited the message!');
            return Redirect::route('admin.contact.show', $id);
        }*/
        $model = Company::find($id);
        $model->update($input);
        Session::flash('message', 'Successfully edited the company!');
        return Redirect::route('company.show', $id);

        /*return Redirect::route('admin.contact.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
        Session::flash('message', 'Er waren enkele errors');*/
    }


}
