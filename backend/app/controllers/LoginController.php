<?php

class LoginController extends \BaseController
{

    public function getIndex()
    {
        return View::make('login');
    }

    public function postIndex()
    {
        if (Auth::attempt(array('username' => Input::get('username'), 'password' => Input::get('password')))) {

            Auth::user()->lastlogin_at = new DateTime();
            Auth::user()->save();
            return Redirect::route('index');
        } else {
            return Redirect::to('login')
                ->with('message', 'Username and password did not match')
                ->withInput(Input::except('password'));

        }
    }

    public function getPasswordreminder()
    {
        return View::make('passwordreminder');
    }

    public function postPasswordreset()
    {

        Password::remind(Input::only('email'), function($message)
        {
            $message->subject('Password Reminder');
        });
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function getLogout()
    {
        //
        Auth::logout();
        return Redirect::to('login');
    }

    public function destroy($id)
    {
        //
        // delete
        $model = User::find($id);
        $model->delete();

        // redirect
        Session::flash('message', 'User werd verwijderd.');
        return Redirect::route('admin.user.index');
    }


}
