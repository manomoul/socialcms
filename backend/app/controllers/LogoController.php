<?php

class LogoController extends \BaseController
{

    /**
     * Update the specified resource in storage.
     *
     * @return Response
     */
    public function postChangeLogo()
    {
        //
        // Build the input for our validation
        $file = Input::file('logo');
        $input = array('logo' => $file);

        // Within the ruleset, make sure we let the validator know that this
        // file should be an image
        $rules = array(
            'logo' => 'required|image|max:2000'
        );

        // Now pass the input and rules into the validator
        $validator = Validator::make($input, $rules);


        // Check to see if validation fails or passes
        if ($validator->fails()) {
            return Redirect::back()->with('message', 'Error: The provided file was not an image or too big');
        } else {
            //var_dump($file);
            $img = Image::make($_FILES['logo']['tmp_name']);
            $img->resize(null, 150, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->save('img/logo.png');
            $img->resize(16, 16, function ($constraint) {});

            $img->save('img/favicon.png');
            File::move('img/favicon.png', 'img/favicon.ico');
            //var_dump($img);
            return Redirect::back()->with('message', 'Success: File upload was successful');
        }
    }


}
