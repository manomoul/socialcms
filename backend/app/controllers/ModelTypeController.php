<?php

class ModelTypeController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
    /*$model = ModelType::where('name', 'post')->first();
        $modelname = 'post';
        $this->layout->content = View::make($modelname . 's.index')->with(array('model' => $model, 'modelname' => $modelname));*/
        $models = Post::paginate(15);
        $modelname = 'post';
        $this->layout->content = View::make($modelname . 's.index')->with(array('models' => $models, 'modelname' => $modelname));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        $modelname = 'post';
        $this->layout->content = View::make('posts.create')->with(array(
            'modelname' => $modelname
        ));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        //
        $input = Input::except(array('_method', '_token', 'photos'));

        $validation = Validator::make($input, Post::$rules);

        if ($validation->passes()) {
            $post = new Post($input);
            $post->save();
            $files = Input::file('photos');

            if ($files[0] !== null) {
                $photos = array();
                foreach ($files as $file) {
                    $rules = array(
                        'file' => 'image',
                    );
                    $validator_img = Validator::make(array('file' => $file), $rules);
                    if ($validator_img->passes()) {
                        $destinationPath = 'img/post/' . $post->id . '/';
                        $filename = str_random(12) . '.' . $file->getClientOriginalExtension();
                        $file->move($destinationPath, $filename);
                        $photo = new Photo();
                        $photo->url = $destinationPath . $filename;
                        $photo->alt = $post->title;
                        //$photo->save();
                        array_push($photos, $photo);
                    } else {
                        return Redirect::back()
                            ->withInput()
                            ->withErrors($validator_img);
                    }
                }
                $post->images()->saveMany($photos);
            }
            /* social tasks */
            foreach (ServiceAction::all() as $serviceaction) {
                $attachments = [];
                foreach ($serviceaction->parameters()->get() as $parameter) {
                    $attachments[$parameter->name] = $parameter->value;
                    if (strpos($parameter->value, '#')) {
                        $pattern = '#.+#';
                        $replacement = "http://www.google.com";
                        preg_replace($pattern, $replacement, $parameter->value);
                    }
                    $attachments[$parameter->name] = $parameter->value;
                }
                $url = $serviceaction->url . '?';

                foreach ($serviceaction->service()->first()->parameters() as $serviceparameter) {
                    $url = $url . $serviceparameter->name . '=' . $serviceparameter->value . '&';
                }
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $attachments);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  //to suppress the curl output
                $result = curl_exec($ch);
                curl_close($ch);
            }

            //return Redirect::back();
            Session::flash('message', 'Successfully posted');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
        $model = ModelType::findorfail($id);
        $modelname = 'model';
        $models = ($model->name == 'post')? Post::Paginate(15) : Agendaitem::Paginate(15);

        $serviceactions =  ServiceAction::where('id', $id)
        ->orderBy('name', 'desc')
        ->take(10)
        ->get();
        $this->layout->content = View::make('modeltypes.show')->with(array('model' => $model, 'modelname' => $modelname, 'models' => $models));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //

        $model = Post::findorfail($id);
        $modelname = 'post';
        $this->layout->content = View::make($modelname . 's.edit')->with(array('model' => $model, 'modelname' => $modelname));
        //return Response::view($modelname.'.edit')->with(array('model' => $model, 'modelname' => $modelname));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //var_dump(Input::all());
        $input = Input::except(array('_method', '_token'));
        /*$validation = Validator::make($input, Contact::$rules);
        if ($validation->passes()) {
            $model = Contact::find($id);
            $model->update($input);
            Session::flash('message', 'Successfully edited the message!');
            return Redirect::route('admin.contact.show', $id);
        }*/
        $model = Post::find($id);
        $model->update($input);
        Session::flash('message', 'Successfully edited the post!');
        return Redirect::route('post.show', $id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        //
        // delete
        $model = Post::find($id);
        $model->delete();

        // redirect
        Session::flash('message', 'The post was deleted');
        return Redirect::route('post.index');
    }


}
