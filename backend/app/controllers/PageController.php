<?php

class PageController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $models = Page::paginate(15);
        $modelname = 'page';
        $this->layout->content = View::make('pages.index')->with(array('models' => $models, 'modelname' => $modelname));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $modelname = 'page';
        $this->layout->content = View::make('pages.create')->with(array(
            'modelname' => $modelname,
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $input = Input::except(array('_method', '_token'));
        $validation = Validator::make($input, Page::$rules);
        echo(var_dump($input));
        if ($validation->passes()) {
            Page::create($input);
            return Redirect::route('user.index');
            Session::flash('message', 'Successfully posted');
        }

        return Redirect::back()
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
        Session::flash('message', 'There were errors');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
        $model = Page::findorfail($id);
        $modelname = 'page';
        $this->layout->content = View::make($modelname . 's.show')->with(array('model' => $model, 'modelname' => $modelname));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //

        $model = Page::findorfail($id);
        $modelname = 'page';
        $this->layout->content = View::make($modelname . 's.edit')->with(array('model' => $model, 'modelname' => $modelname));
        //return Response::view($modelname.'.edit')->with(array('model' => $model, 'modelname' => $modelname));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //var_dump(Input::all());
        $input = Input::except(array('_method', '_token'));
        /*$validation = Validator::make($input, Contact::$rules);
        if ($validation->passes()) {
            $model = Contact::find($id);
            $model->update($input);
            Session::flash('message', 'Successfully edited the message!');
            return Redirect::route('admin.contact.show', $id);
        }*/
        $model = Page::find($id);
        $model->update($input);
        Session::flash('message', 'Successfully edited the page!');
        return Redirect::route('page.show', $id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        // delete
        $model = Page::find($id);
        $model->delete();

        // redirect
        Session::flash('message', 'The page was deleted');
        return Redirect::route('page.index');
    }

}
