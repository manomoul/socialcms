<?php

class ParameterController extends \BaseController
{

    /**
     * Display a listing of the resource.
     * @param $id
     * @return Response
     */
    public function store()
    {


        $input = Input::except(array('_method', '_token'));

        $validation = Validator::make($input, Parameter::$rules);

        if ($validation->passes()) {
            Parameter::create($input);
            return Redirect::back();
        }
        return Redirect::back()
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
        Session::flash('message', 'There were errors');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $model = Parameter::find($id);
        $model->delete();
        return Redirect::back();
    }


}
