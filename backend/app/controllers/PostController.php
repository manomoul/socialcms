<?php

class PostController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        /*$model = ModelType::where('name', 'post')->first();
            $modelname = 'post';
            $this->layout->content = View::make($modelname . 's.index')->with(array('model' => $model, 'modelname' => $modelname));*/
        $models = Post::paginate(15);
        $modelname = 'post';
        $this->layout->content = View::make($modelname . 's.index')->with(array('models' => $models, 'modelname' => $modelname));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        $modelname = 'post';
        $this->layout->content = View::make('posts.create')->with(array(
            'modelname' => $modelname
        ));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        //
        $input = Input::except(array('_method', '_token', 'photos'));

        $validation = Validator::make($input, Post::$rules);

        if ($validation->passes()) {
            $post = new Post($input);
            $post->save();
            $files = Input::file('photos');
            /*
             * Store photos
             * */
            if ($files[0] !== null) {
                $photos = array();
                foreach ($files as $file) {
                    $rules = array(
                        'file' => 'image',
                    );
                    $validator_img = Validator::make(array('file' => $file), $rules);
                    if ($validator_img->passes()) {
                        $destinationPath = 'img/post/' . $post->id . '/';
                        $filename = str_random(12) . '.' . $file->getClientOriginalExtension();
                        $file->move($destinationPath, $filename);
                        $photo = new Photo();
                        $photo->url = $destinationPath . $filename;
                        $photo->alt = $post->title;
                        //$photo->save();
                        array_push($photos, $photo);
                    } else {
                        return Redirect::back()
                            ->withInput()
                            ->withErrors($validator_img);
                    }
                }
                $post->images()->saveMany($photos);
            }
            /*
            * social tasks
             * */
            $modeltype = ModelType::where('name', 'post')->first();

            $serviceactions = $modeltype->serviceactions;

            foreach ($serviceactions as $serviceaction) {
                /*
                 * Add Service parameters
                 * **/
                foreach ($serviceaction->service->parameters as $serviceparameter) {
                    $url = $serviceaction->url . '?';
                    $url = $url . $serviceparameter->name . '=' . $serviceparameter->value . '&';
                }

                $attachments = [];
                foreach ($serviceaction->parameters as $parameter) {
                    //die(var_dump($parameter));
                    //$attachments[$parameter->name] = $parameter->value;
                    $parametervalue = $parameter->value;
                    /*
                     * replace parameters
                     * **/

                    // haal <%param%> eruit
                    $pattern = '<%.+%>';
                    preg_match($pattern, $parameter->value, $matches);

                    if(sizeof($matches)>0) {
                        foreach($matches as $match => $value) {
                            //haal de parameter uit <%parameter%>
                            $key = substr($value, 1, (strlen($value)-2));
                            //vervang de <%parameter%> door de parameterwaarde
                            $parametervalue = str_replace("<%".$key."%>", $post->$key, $parametervalue);
                        }

                    }
                    $url = $url . '&'.$parameter->name."=".str_replace(" ", "%20", $parametervalue);
                    $attachments[$parameter->name] = $parameter->value;
                }

                /*
                 *
                 * $string = 'April 15, 2003';
$pattern = '/(\w+) (\d+), (\d+)/i';
$replacement = '${1}1,$3';
echo
                 * */

                //die(var_dump($url));
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                curl_setopt($ch, CURLOPT_POST, true);
                //curl_setopt($ch, CURLOPT_POSTFIELDS, $attachments);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  //to suppress the curl output
                $result = curl_exec($ch);
                curl_close($ch);
            }

            //return Redirect::back();
            Session::flash('message', 'Successfully posted');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
        $model = Post::findorfail($id);
        $modelname = 'post';
        $this->layout->content = View::make($modelname . 's.show')->with(array('model' => $model, 'modelname' => $modelname));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //

        $model = Post::findorfail($id);
        $modelname = 'post';
        $this->layout->content = View::make($modelname . 's.edit')->with(array('model' => $model, 'modelname' => $modelname));
        //return Response::view($modelname.'.edit')->with(array('model' => $model, 'modelname' => $modelname));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //var_dump(Input::all());
        $input = Input::except(array('_method', '_token'));
        /*$validation = Validator::make($input, Contact::$rules);
        if ($validation->passes()) {
            $model = Contact::find($id);
            $model->update($input);
            Session::flash('message', 'Successfully edited the message!');
            return Redirect::route('admin.contact.show', $id);
        }*/
        $model = Post::find($id);
        $model->update($input);
        Session::flash('message', 'Successfully edited the post!');
        return Redirect::route('post.show', $id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        //
        // delete
        $model = Post::find($id);
        $model->delete();

        // redirect
        Session::flash('message', 'The post was deleted');
        return Redirect::route('post.index');
    }


}