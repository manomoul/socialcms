<?php

class ServiceActionController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        $models = ServiceAction::paginate(15);
        $modelname = 'serviceaction';
        $this->layout->content = View::make($modelname . 's.index')->with(array('models' => $models, 'modelname' => $modelname));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $input = Input::except(array('_method', '_token'));

        $validation = Validator::make($input, ServiceAction::$rules);

        if ($validation->passes()) {
            ServiceAction::create($input);
            return Redirect::back();
            Session::flash('message', 'Successfully posted');
        }
        return Redirect::back()
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
        Session::flash('message', 'There were errors');
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $model = ServiceAction::find($id);
        $model->delete();
        return Redirect::back();
    }

}
