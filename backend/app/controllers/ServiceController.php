<?php

class ServiceController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        $models = Service::paginate(15);
        $modelname = 'service';
        $this->layout->content = View::make('services.index')->with(array('models' => $models, 'modelname' => $modelname));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        $modelname = 'service';
        $this->layout->content = View::make($modelname . 's.create')->with(array('modelname' => $modelname));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
        $model = Service::findorfail($id);
        $modelname = 'service';
        $this->layout->content = View::make($modelname . 's.show')->with(array('model' => $model, 'modelname' => $modelname));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


}
