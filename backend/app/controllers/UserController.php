<?php

class UserController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        $models = User::paginate(15);
        $modelname = 'user';
        $this->layout->content = View::make($modelname . 's.index')->with(array('models' => $models, 'modelname' => $modelname));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $modelname = 'user';
        $this->layout->content = View::make($modelname . 's.create')->with(array('modelname' => $modelname));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $input = Input::except(array('_method', '_token'));

        $validation = Validator::make($input, User::$rules);
        $password = Hash::make(Input::get('password'));

        if ($validation->passes()) {
            User::create(array('username' => Input::get('username'), 'email' => Input::get('email'), 'password' => $password));
            return Redirect::route('user.index');
            Session::flash('message', 'Successfully posted');
        }
        return Redirect::back()
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
        Session::flash('message', 'There were errors');
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
        $modelname = "user";
        $this->layout->content = View::make($modelname . 's.show')->with(array('modelname' => $modelname));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $model = User::find($id);
        $model->delete();

        // redirect
        Session::flash('message', 'The user was deleted');
        return Redirect::route('user.index');
    }


}
