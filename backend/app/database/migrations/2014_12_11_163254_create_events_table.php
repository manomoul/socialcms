<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//

		Schema::create('events', function (Blueprint $table) {
		
		//fk
			$table->increments('id');
			$table->string('title');
			$table->string('slug');
			$table->longText('description')->nullable();
			$table->dateTime('published_time');
			//$table->boolean('confirmed');
			//timestamps
			$table->timestamps();
			$table->softDeletes();
			$table->string('location')->nullable();
			$table->dateTime('starting_time');
			$table->dateTime('ending_time');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('events');
	}

}
