<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('users', function (Blueprint $table) {
				//
				$table->increments('id');

				$table->string('username', 100)->unique();
				$table->string('email', 255)->unique();
				$table->string('password');
				//oauth
				$table->string('oauth_token')->nullable();
				$table->string('oauth_token_secret')->nullable();
				//remember token
				$table->rememberToken();
				//fk
				$table->integer('image_id')->nullable();
				//timestamps
				$table->timestamp('lastlogin_at')
				->nullable();
				$table->timestamps();
				$table->softDeletes();

			});

		//
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		//
		Schema::drop('users');
	}

}