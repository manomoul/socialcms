<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		//
		Schema::create('pages', function (Blueprint $table) {
			//
			$table->increments('id');
			$table->string('title', 35);
			$table->integer('weight')->default(1);
			$table->longText('description');
			$table->string('slug', 100)->unique();
			$table->string('keywords');
			$table->string('author')->default('notmadebymanon.com');
			$table->string('revisit_after')->default('3 month');
			$table->string('robots', 255)->default('index, follow');
			$table->longText('content')->nullable();
			//timestamps
			$table->timestamps();
			$table->softDeletes();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		//
		Schema::drop('pages');
	}


}
