<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		//
		Schema::create('photos', function (Blueprint $table) {
				//
				$table->increments('id');
				$table->string('alt', 128);
				$table->integer('photoable_id')->unsigned();
				$table->string('photoable_type');

				//timestamps
				$table->timestamps();
				$table->softDeletes();

			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		//
		Schema::drop('photos');
	}

}
