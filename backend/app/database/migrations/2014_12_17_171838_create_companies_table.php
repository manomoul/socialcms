<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('companies', function (Blueprint $table) {
				//
				$table->increments('id');
				$table->string('name', 128);
				$table->string('logo_url', 255);
				$table->string('website_url', 255);
				$table->string('address', 255);
				

				//timestamps
				$table->timestamps();
				$table->softDeletes();

			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('companies');
	}

}
