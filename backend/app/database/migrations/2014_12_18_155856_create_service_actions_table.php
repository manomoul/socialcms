<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceActionsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('serviceactions', function (Blueprint $table) {
				//
				$table->increments('id');
				$table->string('name', 128);
				$table->text('description');
				$table->string('url', 255);
				$table->enum('request_type', array('POST', 'GET', 'PUT'));

				//fk
				$table->integer('service_id');

				//timestamps
				$table->timestamps();
				$table->softDeletes();

			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('serviceactions');
	}

}
