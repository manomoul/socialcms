<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('services', function (Blueprint $table) {
				//
				$table->increments('id');
				$table->string('name', 128);
				$table->string('logo_url', 255);
				$table->string('website_url', 255);
				$table->string('host', 255);
				$table->boolean('activated')->default(false);
				$table->text('activate_html');
				

				//timestamps
				$table->timestamps();
				$table->softDeletes();

			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('services');
	}

}
