<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelServiceactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('model_serviceactions', function (Blueprint $table) {
				//fk
				$table->integer('model_id');
				$table->integer('serviceaction_id');

				//timestamps
				$table->timestamps();
				$table->softDeletes();

			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('model_serviceactions');
	}

}
