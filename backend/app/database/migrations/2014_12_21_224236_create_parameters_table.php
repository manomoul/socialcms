<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParametersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('parameters', function (Blueprint $table) {
				//
				$table->increments('id');
				$table->string('name', 128);
				$table->string('value', 255);
				$table->boolean('hidden')->default(false);
				$table->integer('parameterable_id')->unsigned();
				$table->string('parameterable_type');

				

				//timestamps
				$table->timestamps();
				$table->softDeletes();

			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('parameters');
	}

}
