<?php

class InitialCompanySeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		Company::create(array(
				'name' => 'Not Made by Manon',
				'logo_url' => 'img/logo.png',
				'address' => 'Dammekensstraat 16, 9600 Ronse, België',
				'website_url' => 'http://www.notmadebymanon.dev',
			));
	}

}
