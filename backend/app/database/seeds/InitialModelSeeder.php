<?php

class InitialModelSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$models = array(
			1 => array(
				'name'  => 'Post',
				'display_name'  => 'blogpost'),
			2 => array(
				'name'  => 'Event',
				'display_name'  => 'event'),
			); 
		foreach ($models as $i => $value) {
			Model::create(array(
				'name' => $value['name'],
				'display_name' => $value['display_name'],
				));
		}

	}

}
