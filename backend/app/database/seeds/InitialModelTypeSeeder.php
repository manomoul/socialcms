<?php

class InitialModelTypeSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$modeltypes = array(
			1 => array(
				'name'  => 'post',
				'display_name'  => 'blogpost'),
			2 => array(
				'name'  => 'event',
				'display_name'  => 'event'),
			); 
		foreach ($modeltypes as $i => $value) {
			ModelType::create(array(
				'name' => $value['name'],
				'display_name' => $value['display_name'],
				));
		}

	}

}
