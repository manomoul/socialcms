<?php

class InitialSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		User::create(array(
				'username' => 'superadmin',
				'password' => Hash::make('Miq32#Q2RC4pJAA'),
				'email' => 'manon.moulin@gmail.com'
			));
		$this->call('InitialCompanySeeder');
		$this->call('InitialModelTypeSeeder');
		$this->call('InitialServiceSeeder');
	}

}
