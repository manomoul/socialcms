<?php

class InitialServiceSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		$facebook_service = Service::create(array(
			'name' => 'Facebook',
			'logo_url' => 'img/services/facebook.png',
			'website_url' => 'http://www.facebook.com',
			'host' => 'graph.facebook.com',
			'activate_html' => '<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/nl_BE/sdk.js#xfbml=1&appId=872862016079923&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, "script", "facebook-jssdk"));</script>
<div class="fb-login-button" data-scope="publish_actions manage_pages publish_stream" data-max-rows="1" data-size="xlarge" data-show-faces="false" data-auto-logout-link="false"></div>',
			'activated' => true,
			));

		$parameters = array(
			1 => array(
				'name'  => 'App_secret',
				'value'  => '8dbc897ced0c1522b65933c40014bf23'),
			2 => array(
				'name'  => 'App_id',
				'value'  => '872862016079923'),
			3 => array(
				'name' => 'access_token',
				'value'  => '872862016079923'),
			); 

		/*foreach ($parameters as $i => $value) {
			ServiceParameter::create(array(
				'name' => $value['name'],
				'value' => $value['value'],
				'service_id' => $facebook_service->id,
				));
		}*/

		//service actions

		
			ServiceAction::create(array(
				'name' => 'create post',
				'description' => 'post to facebook page feed',
				'url' => '/v2.2/405914656215858/feed',
				'request_type' => 'POST',
				'service_id' => $facebook_service->id,
				));
		

		

		
		
	}

}
