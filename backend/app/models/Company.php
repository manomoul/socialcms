<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Company extends Eloquent {

	use SoftDeletingTrait;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table     = 'companies';
	protected $dates     = ['deleted_at'];
	protected $fillable = array('name', 'website_url', 'address');


	/*protected $dates      = ['deleted_at'];*/
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	public function images() {
		return $this->morphMany('Image', 'imageable');
	}

}
