<?php

class Agendaitem extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table     = 'events';
	public $timestamps = false;
	protected $fillable = array('title', 'description', 'published_time', 'location', 'starting_time', 'ending_time');
	public static $rules = array(
		'title' => 'required|min:2',
		'description' => 'required|min:2',
		'published_time' => 'date',
		'starting_time' => 'required|date',
		'ending_time' => 'required|date'
	);

	public function images()
	{
		return $this->morphMany('Photo', 'photoable');
	}

}

