<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class ImageUnit extends Eloquent {

	use SoftDeletingTrait;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table     = 'imageunits';
	protected $dates     = ['deleted_at'];
	/*public static $rules = array(
		'title'          => 'required',
		'intro'         => 'required',
		'starting_time' => 'required|date',
		'ending_time'   => 'required|date',

	);*/

	/*protected $dates      = ['deleted_at'];*/

	public function image()
	{
		return $this->belongsTo('Photo');
	}
}
