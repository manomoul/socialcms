<?php

class Message extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table     = 'messages';
    protected $dates     = ['deleted_at'];
    protected $fillable = array('name', 'email', 'message');
    public static  $rules = array(
        'name' => 'required|min:2',
        'email' => 'required|email',
        'message' => 'required|min:2',
    );
}

