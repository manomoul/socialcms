<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class ModelType extends Eloquent {

	use SoftDeletingTrait;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table     = 'model_types';
	protected $dates     = ['deleted_at'];


	/*protected $dates      = ['deleted_at'];*/
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	public function serviceactions()
    {
		return $this->belongsToMany('ServiceAction', 'model_serviceactions', 'model_id', 'serviceaction_id');
    }

	public function getModels()
	{
		return Post::all();
	}
}
