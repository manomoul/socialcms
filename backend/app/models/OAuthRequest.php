<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class OAuthRequest extends Eloquent {

	use SoftDeletingTrait;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table     = 'model_serviceactions';
	protected $dates     = ['deleted_at'];


	/*protected $dates      = ['deleted_at'];*/
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	
	public function model()
    {
        return $this->belongsTo('Model');
    }

    public function serviceaction()
    {
        return $this->belongsTo('ServiceAction');
    }
}
