<?php

class Page extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table     = 'pages';
	protected $dates     = ['deleted_at'];
	protected $fillable = array('title', 'description', 'content', 'keywords', 'slug', 'weight');
	public static  $rules = array(
		'title' => 'required|min:2',
		'description' => 'required',
	);
}

