<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Parameter extends Eloquent {

	use SoftDeletingTrait;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table     = 'parameters';
	protected $dates     = ['deleted_at'];
	protected $fillable = array('name', 'value', 'parameterable_id', 'parameterable_type');
	public static  $rules = array(
		'name' => 'required',
		'value' => 'required'
	);


	/*protected $dates      = ['deleted_at'];*/
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	public function parameterable()
	{
		return $this->morphTo();
	}


}
