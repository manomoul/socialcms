<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Photo extends Eloquent {

	use SoftDeletingTrait;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table     = 'photos';
	protected $dates     = ['deleted_at'];
	/*public static $rules = array(
		'title'          => 'required',
		'intro'         => 'required',
		'starting_time' => 'required|date',
		'ending_time'   => 'required|date',

	);*/

	/*protected $dates      = ['deleted_at'];*/
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	public function photoable()
	{
		return $this->morphTo();
	}

	public function imageunits()
	{
		return $this->hasMany('ImageUnit');
	}

}
