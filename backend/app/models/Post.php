<?php


class Post extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table     = 'posts';
	protected $fillable = array('title', 'description', 'published_time');
	public static  $rules = array(
		'title' => 'required|min:2',
		'description' => 'required',
		'published_time' => 'date',
	);

	public function images()
	{
		return $this->morphMany('Photo', 'photoable');
	}

}

