<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Service extends Eloquent {

	use SoftDeletingTrait;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table     = 'services';
	protected $dates     = ['deleted_at'];


	/*protected $dates      = ['deleted_at'];*/
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	public function parameters()
	{
		return $this->morphMany('Parameter', 'parameterable');
	}

	public function actions()
	{
		return $this->hasMany('ServiceAction');
	}

}
