<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class ServiceAction extends Eloquent {

	use SoftDeletingTrait;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table     = 'serviceactions';
	protected $dates     = ['deleted_at'];
	protected $fillable = array('name', 'description', 'url', 'request_type', 'service_id', 'modelname');
	public static  $rules = array(
		'name' => 'required',
		'description' => 'required',
		'url' => 'required',
		'request_type' => 'required',
	);


	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	public function service()
	{
		return $this->belongsTo('Service');
	}

	public function parameters() {
		return $this->morphMany('Parameter', 'parameterable');
	}

}
