<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class ServiceParameter extends Eloquent {

	use SoftDeletingTrait;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table     = 'service_parameters';
	protected $dates     = ['deleted_at'];


	/*protected $dates      = ['deleted_at'];*/
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	public function service()
    {
        return $this->belongsTo('Sevice');
    }

}
