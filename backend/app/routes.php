<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/



/* Login */
//Route::group(array('before'  => 'guest', array('only' => 'getIndex')), function () {
Route::controller('/login', 'LoginController');
Route::controller('/password', 'RemindersController');
Route::controller('/api/v1', 'ApiController');

//});

Route::group(array('before'  => 'auth'), function () {
	
	/* Home */
	//Route::get('/configuration', array('as'                 => 'configuration', 'uses'                 => 'CompanyController@show', 'with' => array('id' => 1)));
	Route::get('/', array('as'                 => 'index', 'uses'                 => 'HomeController@index'));
	Route::get('/sitemap.xml', array('sitemap' => 'index', 'uses' => 'HomeController@sitemap'));

	/* Resources */
	Route::controller('/logo', 'LogoController');
	Route::resource('parameter', 'ParameterController');
	Route::resource('image', 'ImageController');
	Route::resource('page', 'PageController');
	Route::resource('event', 'EventController');
	Route::resource('service', 'ServiceController');
	Route::resource('post', 'PostController');
	Route::resource('model', 'ModelTypeController');
	Route::resource('serviceaction', 'ServiceActionController');
	Route::resource('user', 'UserController');
	Route::resource('company', 'CompanyController',
		array('only' => array('show', 'edit', 'update')));
	Route::resource('oauthrequest', 'OAuthRequestController',
		array('only' => array('store')));

	Route::post('/oauthrequest', array('as'                 => 'oauthrequest', 'uses'                 => 'OAuthRequestController@create'));
	Route::resource('parameter', 'ParameterController');

	Route::get('/img_og/{id?}', array('as' => 'image', function ($id = null) {
		//
		$img_url = ($id) ? Photo::find($id) : asset('img/logo.png');
		$img = Image::make($img_url)->fit(1200, 630, function ($constraint) {
			$constraint->aspectRatio();
			//$constraint->upsize();
		});
		return $img->response('jpg');


	}));

	Route::get('/image/{id}/{width}/{height?}', array('as' => 'image', function ($id, $width, $height = null) {
		//
		$img_url = ($id) ? Photo::find($id) : asset('img/logo.png');
		$img = Image::make($img_url)->fit($width, $height, function ($constraint) {
			$constraint->aspectRatio();
			//$constraint->upsize();
		});
		return $img->response('jpg');
	}));
});


