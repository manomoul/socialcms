<div class="col-lg-10 col-md-9 col-sm-8">
     <ul>
         @foreach($errors->all() as $error)
             <li>{{ $error }}</li>
         @endforeach
     </ul>
     {{ Form::model($model, array('route' => array($modelname.'.update', $model->id), 'method' => 'PUT')) }}
    {{ Form::submit('Opslaan', array('class' => 'btn btn-success btn-lg pull-right')) }}
     <table class="table">
         <tr><th>{{ Form::label('name', 'Name') }}</th><td>{{ Form::text('name', null, array('class' => 'form-control')) }}</td></tr>
         <tr><th>Logo</th><td><img src="{{{ asset($model->logo_url) }}}" class="img-responsive" alt="logo" />
                 <button type="button" class="btn btn-primary btn-lg form-control" data-toggle="modal" data-target="#changeLogo">
                     Change logo
                 </button>
             </td></tr>
         <tr><th>{{ Form::label('website_url', 'Website') }}</th><td>{{ Form::text('website_url', null, array('class' => 'form-control')) }}</td></tr>
         <tr><th>{{ Form::label('address', 'Address') }}</th><td>{{ Form::text('address', null, array('class' => 'form-control')) }}</td></tr>
     </table>
     {{ Form::close() }}
 </div>



<!-- Modal -->
<div class="modal fade" id="changeLogo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        {{ Form::open(array('action' => 'LogoController@postChangeLogo', 'files' => true)) }}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Change your logo</h4>
            </div>
            <div class="modal-body" ng-controller="ImageDragController as image">
                <!--<div class="dragImage hidden-xs" data-drop="true" id="div1" ondrop="image.drop(event)" ondragover="image.allowDrop(event)">
                    Drag your image here
                </div>-->

                <table class="table">

                    {{ Form::file('logo') }}

                </table>



            </div>
            <div class="modal-footer">
                {{ Form::submit('Opslaan', array('class' => 'btn btn-primary')) }}

            </div>

        </div>
        {{ Form::close() }}
    </div>
</div>




