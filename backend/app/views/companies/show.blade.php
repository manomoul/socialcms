
<!-- Table -->
<div class="col-lg-10 col-md-9 col-sm-8">

  <table class="table">
   <tr><th>Name</th><td>{{{ $model->name }}}</td></tr>
   <tr><th>Logo</th><td><img src="{{{ asset($model->logo_url) }}}" class="img-responsive" alt="logo" /></td></tr>
   <tr><th>Website</th><td><a href="{{{ $model->website_url }}}">{{{ $model->website_url }}}</a></td></tr>
      <tr><th>Address</th><td>{{{ $model->address }}}</td></tr>
 </table>
</div>
<!-- Table -->
     <div class="col-xs-12 col-sm-8 col-md-3 col-lg-2">
         @include('custom_controls.btn_edit', array('url'=>route($modelname.'.edit', $model->id)))
     </div>
