<a href="{{ $url }}" class="btn btn-success btn-sm">
    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create new record
</a>
<br />