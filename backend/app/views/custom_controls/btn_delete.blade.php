{{ Form::open(array('url' => $url, 'class' => 'pull-left')) }}
{{ Form::hidden('_method', 'DELETE') }}
<button type="submit" class="btn btn-danger">
    <i class="glyphicon glyphicon-remove"></i>Delete
</button>
{{ Form::close() }}