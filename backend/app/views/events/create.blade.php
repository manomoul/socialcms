<!-- Table -->

@include('partials.breadcrumbs', array('title'=>'Create new record', 'modelname'=> $modelname))
<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
 {{ Form::model( $event, array('url'=>route('event.store'), 'files' =>true)) }}
        
        <table class="table">
          <tr><th>{{ Form::label('title', 'Title') }}</th><td>{{ Form::text('title', null, array('class' => 'form-control')) }}</td></tr>
          <tr><th>{{ Form::label('intro', 'Intro') }}</th><td>{{ Form::textarea('intro', null, array('class' => 'form-control')) }}</td></tr>
          <tr><th>{{ Form::label('description', 'description') }}</th><td>{{ Form::textarea('description', null, array('class' => 'form-control')) }}</td></tr>
          <tr><th>{{ Form::label('location', 'Location') }}</th><td>{{ Form::text('location', null, array('class' => 'form-control')) }}</td></tr>
          <tr><th>{{ Form::label('starting_time', 'Begin') }}</th>
              <td><div class='input-group date datetimepicker'>
                    <input name="starting_time" type='datetime' class="form-control" data-datepicker="datepicker"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
              </td>
          </tr>
        <tr><th>{{ Form::label('ending_time', 'End') }}</th>
            <td><div class='input-group date datetimepicker'>
                    <input name="ending_time" type='datetime' class="form-control" data-provide="datepicker"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
                </div>
            </td>
        </tr>
            <tr><th>{{ Form::label('img', 'Afbeeldingen') }}</th><td><input type="file" name="photos[]" accept="image/*" multiple></td></tr>
            <tr><th>{{ Form::label('published_time', 'Published time') }}</th>
                <td><div class='input-group date datetimepicker'>
                        <input name="published_time" type='datetime' class="form-control" data-datepicker="datepicker" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
                    </div>
                </td>
            </tr>
        </table>
        {{ Form::submit('Save record', array('class' => 'btn btn-primary')) }}
  {{ Form::close() }}

