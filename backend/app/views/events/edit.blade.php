<ul>
	@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
	@endforeach
</ul>
{{ Form::model($model, array('route' => array($modelname.'.update', $model->id), 'method' => 'PUT')) }}
{{ Form::submit('Opslaan', array('class' => 'btn btn-success btn-lg pull-right')) }}
<table class="table">
	<tr><th>{{ Form::label('title', 'title') }}</th><td>{{ Form::text('title', null, array('class' => 'form-control')) }}</td></tr>
	<tr><th>{{ Form::label('description', 'description') }}</th><td>{{ Form::textarea('description', null, array('class' => 'form-control')) }}</td></tr>
	<tr><th>{{ Form::label('location', 'Location') }}</th><td>{{ Form::text('location', null, array('class' => 'form-control')) }}</td></tr>
	<tr><th>{{ Form::label('starting_time', 'Begin') }}</th>
		<td><div class='input-group date datetimepicker'>
				<input name="starting_time" type='text' class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
			</div>
		</td>
	</tr>
	<tr><th>{{ Form::label('ending_time', 'End') }}</th>
		<td><div class='input-group date datetimepicker'>
				<input name="ending_time" type='text' class="form-control" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
			</div>
		</td>
	</tr>
</table>
{{ Form::close() }}
