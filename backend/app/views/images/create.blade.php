<!-- Table -->

@include('partials.breadcrumbs', array('title'=>'Create new record', 'modelname'=> $modelname))
<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
{{ Form::model( $image, array('url'=>route('image.store'), 'files' =>true)) }}

<table class="table">
    <tr><th>{{ Form::label('title', 'Title') }}</th><td>{{ Form::text('title', null, array('class' => 'form-control')) }}</td></tr>
    <tr><th>{{ Form::label('img', 'Afbeelding') }}</th><td><input type="file" name="photo" ></td></tr>
</table>
{{ Form::submit('Save record', array('class' => 'btn btn-primary')) }}
{{ Form::close() }}

