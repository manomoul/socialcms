<div ng-controller="TabController as tab">
	<ul id="models_tabs" class="nav nav-tabs">

		@for($i = 0; $i < count($models); $i++)
		<li role="presentation" ng-class="{ active: tab.isSet({{$i}}) }"><a ng-click="tab.setTab({{$i}})" href="#tab_{{$i}}" data-toggle="tab">{{{ $models[$i]['display_name'] }}}</a></li>
		@endfor
	</ul>
	<div>

		@for($i = 0; $i < count($models); $i++)

		<div ng-show="tab.isSet({{$i}})" class="tab-pane" id="tab_{{{ $models[$i]['display_name'] }}}">
			@include($models[$i]['name'].'s.create', array('modelname'=> $models[$i]['name'] ))
		</div>

		@endfor
	</div>
</div>
