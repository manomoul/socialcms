<!DOCTYPE html>
<html ng-app="nmbm">
    @include('layouts.partials.head')
    <body>
        <div id="wrapper">
            <!--[if lt IE 7]>
                <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
            <![endif]-->

            <!-- Content -->
                <!-- header -->
                @include('layouts.partials.header')
                <!-- end header -->
                
            <div id="content_wrapper" class="container">

                <!-- message -->    
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                <!-- end message -->

                <!-- content -->
                <section id="content">
                    @if (isset($content))
                        {{ $content }}
                    @endif
                </section>
                <!-- end content -->
            </div>
            <!-- End content -->
        </div>

        @include('layouts.partials.footer')
        <!-- Scripts bottom -->
            @include('layouts.partials.js_bottom')
        <!-- End Scripts bottom -->
    </body>
</html>
