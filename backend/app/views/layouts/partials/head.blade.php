<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>CMS</title>
        <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        {{ HTML::script('bower_components/ckeditor/ckeditor.js') }}
        {{ HTML::script('bower_components/angular/angular.js') }}
        {{ HTML::script('js/vendor/angular-slugify.js') }}
        {{ HTML::script('js/ui-bootstrap.min.js') }}
        {{ HTML::script('js/main.min.js') }}
        {{ HTML::script('js/app.js') }}
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        {{ HTML::style('css/bootstrap.min.css') }}
        {{ HTML::style('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}
        {{ HTML::style('css/main.min.css') }}
        <!--<script src="js/vendor/modernizr-2.6.2.min.js"></script>-->
    </head>

    @show
    <!-- Modernizr -->
