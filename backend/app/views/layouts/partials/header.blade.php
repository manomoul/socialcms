
   <nav class="navbar navbar-inverse">
      <div class="container-fluid">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('index') }}">
               <img alt="Brand" height="20" src="{{ asset('img/logo.png') }}">
            </a>
         </div>

         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
               <li><a href="{{ route('index') }}">Home</a></li>
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Models <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                     <li><a href="{{ route('model.show', 1) }}">Posts</a></li>
                     <li><a href="{{ route('model.show', 2) }}">Events</a></li>
                     {{--<li role="separator" class="divider"></li>
                     <li><a href="#">Separated link</a></li>
                     <li role="separator" class="divider"></li>
                     <li><a href="#">One more separated link</a></li>--}}
                  </ul>
               </li>
               <li><a href="{{ route('page.index') }}">Pages</a></li>
               <li><a href="{{ route('service.index') }}">Services</a></li>
               <li><a href="{{ route('user.index') }}">Users</a></li>
               <li><a href="{{ route('company.show', 1) }}">Configuration</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{Auth::user()->username}} <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                     <li><a href="{{ route('user.show', Auth::user()->id) }}">View my account</a></li>
                     <li role="separator" class="divider"></li>
                     <li><a href="{{ action('LoginController@getLogout')}}"><span class="glyphicon glyphicon-off" aria-hidden="true"></span> Logout</a></li>
                  </ul>
               </li>
            </ul>
         </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
   </nav>
