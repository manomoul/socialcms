@section('js_bottom')
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>

        {{ HTML::script('js/bootstrap.min.js') }}

        {{ HTML::script('js/vendor/moment.min.js') }}
        {{ HTML::script('js/vendor/bootstrap-datetimepicker.js') }}
        <script type="text/javascript">
                $(function () {
                        //2015-08-28 05:25:02
                        $('.datetimepicker').each(function(index) {
                                $(this).datetimepicker({
                                        useCurrent: false,
                                        format: 'YYYY-MM-DD hh:mm:ss'
                                });
                        });

                });
        </script>


@show