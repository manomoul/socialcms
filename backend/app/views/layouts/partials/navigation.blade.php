

<nav class="navbar navbar-default col-lg-2 col-md-3 col-sm-4" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand visible-xs" href="#">Company name</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
   
	    <ul class="nav nav-pills nav-stacked">
      <li role="presentation"><a href="{{ route('index') }}">Home</a></li>
      <li role="presentation" class="dropdown">
         <a data-toggle="collapse" href="#models_collapse">
            Models
         </a>
        <ul id="models_collapse" class="collapse in">  
          <li role="presentation"><a href="{{ route('model.show', 1) }}">Blogposts</a></li>
          <li role="presentation"><a href="{{ route('model.show', 2) }}">Events</a></li>
        </ul>
      </li>
          <li role="presentation"><a href="{{ route('page.index') }}">Pages</a></li>
      <li role="presentation"><a href="{{ route('user.index') }}">Users</a></li>
          <li role="presentation"><a href="{{ route('service.index') }}">Services</a></li>
   


      <hr />
			<li role="presentation"><a href="{{ route('company.show', 1) }}">Configuration</a></li>
		</ul>

  </div><!-- /.container-fluid -->
</nav>