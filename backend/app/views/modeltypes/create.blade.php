<!-- Table -->

@include('partials.breadcrumbs', array('title'=>'Create new record', 'modelname'=> $modelname))
<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
 {{ Form::open(array('url'=>route('post.store'), 'files' =>true)) }}
        
        <table class="table">
          <tr><th>{{ Form::label('title', 'Title') }}</th><td>{{ Form::text('title', null, array('class' => 'form-control')) }}</td></tr>
          <tr><th>{{ Form::label('description', 'Short') }}</th><td>{{ Form::textarea('description', null, array('class' => 'form-control')) }}</td></tr>
            <tr><th>{{ Form::label('published_time', 'Published time') }}</th>
                <td><div class='input-group date datetimepicker'>
                        <input name="published_time" type='text' class="form-control" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
                    </div>
                </td>
            </tr>           <tr><th>{{ Form::label('img', 'Afbeeldingen') }}</th><td><input type="file" name="photos[]" multiple></td></tr>
        </table>
        {{ Form::submit('Save record', array('class' => 'btn btn-primary')) }}
  {{ Form::close() }}

