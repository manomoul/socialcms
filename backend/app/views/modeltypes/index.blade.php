<!-- Table -->

<div class="col-xs-12 col-sm-8 col-md-3 col-lg-2 pull-right">
    @include('custom_controls.btn_create', array('url'=>route($modelname.'.create')))
</div>
<div>
  @if(count($models))
  <table class="table table-striped col-xs-12 col-sm-12 col-md-12 col-lg-12">

    <tr>
     <th>title</th>
     <th class="hidden-xs hidden-sm">description</th>
     <th class="hidden-xs">date</th>
   </tr>

      <tbody>
   @foreach($models as $key => $value)
   <tr class="item">
       <td> <a href="{{ route('post.show', $value->id) }}" role="button">{{{ $value->title }}}</a></td>
     <td class="hidden-xs hidden-sm">{{{ $value->description }}}</td>
     <td class="hidden-xs">{{{ $value->starting_time }}}</td>
 </tr>
 @endforeach
      </tbody>
</table>
@else



        <div class="panel panel-info col-xs-12 col-sm-12 col-md-10 col-lg-10">
            <div class="panel-body">
                There are no records
            </div>
        </div>
@endif
</div>
<div class="row pull-right">
    <div class="pull-right">
        {{ $models->links() }}
    </div>
</div>
