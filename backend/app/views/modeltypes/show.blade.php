<!-- Table -->
@include('partials.breadcrumbs', array('title'=>$model->title, 'modelname'=> $modelname))

    <div class="panel panel-default">
        <div class="panel-heading">Values</div>
        <div class="panel-body">
            @foreach($models->first()->getAttributes() as $key => $value)
               <strong>{{{ $key }}} |</strong>
            @endforeach
            <strong>url</strong>
        </div>
    </div>


<h2>Serviceactions</h2>
@if(count($model->serviceactions))

    <table class="table table-striped col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <tr>
            <th></th>
            <th>title</th>
            <th class="hidden-xs hidden-sm">description</th>
            <th></th>
        </tr>

        <tbody>
        @foreach($model->serviceactions as $key => $value)
            <tr class="item">
                <td> <img width="25" src="{{{ asset($value->service->logo_url) }}}" alt="{{{ $value->service->name }}}" class="img-thumbnail"></td>
                <td> {{{ $value->name }}}</td>
                <td class="hidden-xs hidden-sm">{{{ $value->description }}}</td>
                <td></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else



    <div class="panel panel-info">
        <div class="panel-body">
            There are no records
        </div>
    </div>
@endif
<div class="col-xs-12 col-sm-8 col-md-3 col-lg-2 pull-right">
    @include('custom_controls.btn_create', array('url'=>route($model->name.'.create')))
</div>
@if(count($models))
    <h2>Records</h2>
    <table class="table table-striped col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <tr>
            <th>title</th>
            <th class="hidden-xs hidden-sm">description</th>
            <th class="hidden-xs">created</th>
        </tr>

        <tbody>
        @foreach($models as $key => $value)
            <tr class="item">
                <td> <a href="{{ route('post.show', $value->id) }}" role="button">{{{ $value->title }}}</a></td>
                <td class="hidden-xs hidden-sm">{{{ $value->description }}}</td>
                <td class="hidden-xs">{{{ $value->created_at }}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else



    <div class="panel panel-info col-xs-12 col-sm-12 col-md-10 col-lg-10">
        <div class="panel-body">
            There are no records
        </div>
    </div>
@endif