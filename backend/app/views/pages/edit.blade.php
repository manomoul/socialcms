<!-- Table -->

@include('partials.breadcrumbs', array('title'=>'Create new record', 'modelname'=> $modelname))
<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
{{ Form::model($model, array('route' => array($modelname.'.update', $model->id), 'method' => 'PUT')) }}
{{ Form::submit('Opslaan', array('class' => 'btn btn-success btn-lg pull-right')) }}
        <table class="table">
          <tr><th>{{ Form::label('title', 'Title') }}</th><td>{{ Form::text('title', null, array('class' => 'form-control')) }}</td></tr>
          <tr><th>{{ Form::label('slug', 'Slug') }}</th><td><input class="form-control" placeholder="<% title | slugify %>" value="{{ $model->slug }}" type="text" name="slug"></td></tr>
          <tr><th>{{ Form::label('description', 'description') }}</th><td><textarea class="form-control" name="description" value="{{ $model->description }}">{{ $model->description }}</textarea></td></tr>
            <tr><th>{{ Form::label('weight', 'Weight') }}</th><td>{{ Form::number('weight', $model->weight, array('class' => 'form-control')) }}</td></tr>
            <tr><th>{{ Form::label('content', 'content') }}</th><td><textarea class="ckeditor" name="content" value="{{ $model->content }}">{{ $model->content }}</textarea></td></tr>
          <tr><th>{{ Form::label('keywords', 'keywords') }}</th><td>{{ Form::text('keywords', null, array('class' => 'form-control')) }}</td><td>* seperate by comma</td></tr>
            <tr><th>{{ Form::label('published_time', 'Published time') }}</th>
                <td><div class='input-group date datetimepicker'>
                        <input name="published_time" type='text' class="form-control" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
                    </div>
                </td>
            </tr>
        </table>
        {{ Form::submit('Save record', array('class' => 'btn btn-primary')) }}
  {{ Form::close() }}
