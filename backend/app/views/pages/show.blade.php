<!-- Table -->
@include('partials.breadcrumbs', array('title'=>$model->title, 'modelname'=> $modelname))
<div class="col-xs-12 col-sm-8 col-md-3 col-lg-1 pull-right">
    @include('custom_controls.btn_edit', array('url'=>route($modelname.'.edit', $model->id)))
    @include('custom_controls.btn_delete', array('url'=>route($modelname.'.destroy', $model->id)))
</div>
<div class="col-lg-10 col-md-9 col-sm-8">

    <table class="table">
        <tr><th>Title</th><td>{{{ $model->title }}}</td></tr>
        <tr><th>Description</th><td>{{{ $model->description }}}</td></tr>
        <tr><th>Content</th><td><div class="page-content">{{ $model->content }}</div></td></tr>

    </table>
</div>