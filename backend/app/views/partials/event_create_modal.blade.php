<button type="button" class="btn btn-success btn-lg col-xs-12 col-sm-12 col-md-12 col-lg-12" data-toggle="modal" data-target="#modad_create_event">
      <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create new record
</button>

<!-- Modal -->
<div class="modal fade" id="modad_create_event" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Create new record</h4>
      </div>
      <div class="modal-body">
        <ul>
          @foreach($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
        {{ Form::open(array('url'=>route('event.store'), 'files' =>true)) }}
        
        <table class="table">
          <tr><th>{{ Form::label('title', 'Title') }}</th><td>{{ Form::text('title', null, array('class' => 'form-control')) }}</td></tr>
          <tr><th>{{ Form::label('intro', 'Intro') }}</th><td>{{ Form::textarea('intro', null, array('class' => 'form-control')) }}</td></tr>
          <tr><th>{{ Form::label('description', 'description') }}</th><td>{{ Form::textarea('description', null, array('class' => 'form-control')) }}</td></tr>
          <tr><th>{{ Form::label('location', 'Location') }}</th><td>{{ Form::text('location', null, array('class' => 'form-control')) }}</td></tr>
          <tr><th>{{ Form::label('starting_time', 'Begin') }}</th><td><input type="date" name="starting_date"> <input type="time" name="starting_time"></td></tr>
          <tr><th>{{ Form::label('ending_time', 'Begin') }}</th><td><input type="date" name="ending_date"> <input type="time" name="starting_time"></td></tr>
          <tr><th>{{ Form::label('img', 'Afbeeldingen') }}</th><td><input type="file" name="photos[]" multiple></td></tr>
        </table>

         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {{ Form::submit('Save record', array('class' => 'btn btn-primary pull-right')) }}
        {{ Form::close() }}
      </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>
</div>