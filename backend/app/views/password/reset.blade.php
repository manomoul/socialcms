<!DOCTYPE html>
    @include('layouts.partials.head')
    <body class="login_bg">
        <div id="wrapper">
            <!--[if lt IE 7]>
                <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
            <![endif]-->

            <!-- Content -->
                <!-- header -->
                <header>
                   <img class="center-block" src="{{ asset('img/logo.png') }}" alt="company logo" class="img-circle">
                </header>
                <!-- end header -->
                
                
                <!-- end message -->

                <!-- content -->
                <div class="container">
                  <div class="text-center">     
                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 center-block float-none center-vertical shadow login">
                    
                       
                      {{ Form::open(array('url'=>'passwordreset', 'method'=>'post')) }}
                              

                        <form action="{{ action('RemindersController@postReset') }}" method="POST">
                            <div class="form-group">
                                <input type="hidden" name="token" value="{{ $token }}">
                                <input type="email" name="email">
                                <input type="password" name="password">
                                <input type="password" name="password_confirmation">
                                <input type="submit" value="Reset Password">
                            </div>
                        </form>
                          {{ Form::submit('Send reminder', array('class'=>'btn btn-primary btn-lg btn-block'))}}
                          
                      {{ Form::close() }}

                      <!-- message -->    
                      @if (Session::has('message'))
                          <div class="alert alert-info">{{ Session::get('message') }}</div>
                      @endif

                      </div>
                  </div>
                </div>

                <!-- end content -->
            <!-- End content -->

            <!-- Scripts bottom -->
                @include('layouts.partials.js_bottom')
            <!-- End Scripts bottom -->
        </div>

        @include('layouts.partials.footer')
    </body>
</html>
