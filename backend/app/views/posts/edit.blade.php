<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
{{ Form::model($model, array('route' => array($modelname.'.update', $model->id), 'method' => 'PUT')) }}
{{ Form::submit('Opslaan', array('class' => 'btn btn-success btn-lg pull-right')) }}
<table class="table">
    <tr><th>{{ Form::label('title', 'title') }}</th><td>{{ Form::text('title', null, array('class' => 'form-control')) }}</td></tr>
    <tr><th>{{ Form::label('description', 'description') }}</th><td>{{ Form::textarea('description', null, array('class' => 'form-control')) }}</td></tr>
</table>
{{ Form::close() }}
