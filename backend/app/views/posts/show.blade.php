<!-- Table -->
@include('partials.breadcrumbs', array('title'=>$model->title, 'modelname'=> $modelname))
<div class="col-xs-12 col-sm-8 col-md-3 col-lg-1 pull-right">
    @include('custom_controls.btn_edit', array('url'=>route($modelname.'.edit', $model->id)))
    @include('custom_controls.btn_delete', array('url'=>route($modelname.'.destroy', $model->id)))
</div>
<div class="col-lg-10 col-md-9 col-sm-8">

    <table class="table">
        <tr><th>Title</th><td>{{{ $model->title }}}</td></tr>
        <tr><th>Description</th><td>{{{ $model->description }}}</td></tr>

    </table>
</div>

@foreach($model->images as $key => $value)
    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
        <div class="thumbnail">
            <img src="{{ asset($value->url) }}" alt="{{ $value->alt }}">
            <div class="caption">
                <h3>{{ $value->name }}</h3>

                <table class="table table-bordered">
                    <tr>
                        <td>
                            {{ Form::open(array('url' => route('image.destroy', $value->id), 'class' => ' form-inline')) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            <button type="submit" class="btn btn-danger visible-*-inline">
                                <span class="glyphicon glyphicon-remove"></span>
                            </button>
                            {{ Form::close() }}
                        </td>
                    </tr>
                </table>
                </p>
            </div>
        </div>
    </div>

@endforeach
