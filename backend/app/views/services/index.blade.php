
@foreach($models as $key => $value)
<a class="service" href="{{ route($modelname.'.show', array( 'id' => $value->id)) }}">
	<img src="{{ $value->logo_url }}" alt="{{ $value->name }}" width="150" class="img-thumbnail img-responsive">
</a>
@endforeach

<div class="row">
	<div class="pull-right">
		{{ $models->links() }}
	</div>
</div>
