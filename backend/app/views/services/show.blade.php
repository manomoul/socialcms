<img src="{{ asset($model->logo_url) }}" alt="{{ $model->name }}" width="150" class="img-thumbnail img-responsive">
<table>
    <tr>
        <th>Name</th>
        <td>{{ $model->name }}</td>
    </tr>
    <tr>
        <th>Website</th>
        <td>{{ $model->website_url }}</td>
    </tr>
    <tr>
        <th>Host</th>
        <td>{{ $model->host }}</td>
    </tr>
    @if($model->activated)
        <tr>
            <th>Activated</th>
            <td>Yes</td>
        </tr>
    @else
        <tr>
            <th>Activate</th>
            <td>{{ $model->activate_html }}</td>
        </tr>
    @endif

</table>

<h2>Parameters</h2>
<div class="row">

    {{ Form::open(array('url'=>route('parameter.store'))) }}
    {{ Form::hidden('parameterable_id', $model->id); }}
    {{ Form::hidden('parameterable_type', 'service'); }}
    <div class="col-lg-2">
        <label>Name</label>
    </div>
    <div class="col-lg-3">
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>
    <div class="col-lg-2">
        <label>Value</label>
    </div>
    <div class="col-lg-3">
        {{ Form::text('value', null, array('class' => 'form-control')) }}
    </div>
    <div class="col-lg-2">
        {{ Form::submit('Save record', array('class' => 'btn btn-primary pull-right')) }}
    </div>

    {{ Form::close() }}
</div><!-- /.row -->

@if(count(!$model->parameters))
    <table class="table col-lg-12 table-striped">
        @foreach($model->parameters as $key => $parameter)
            <tr>
                <td>{{ $parameter->name }}</td>
                <td>{{ $parameter->value }}</td>
                <td width="20">
                    {{ Form::open(array('url' => route('parameter.destroy', $parameter->id), 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    <button type="submit" class="btn btn-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                    </button>
                    {{ Form::close() }}
                </td>
            </tr>
        @endforeach
    </table>
@endif

<h2>Actions</h2>
<div class="row">
    <!-- Button trigger modal -->
    <div class="col-lg-12">
        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#createActionModal">
            Add new
        </button>
    </div>
</div><!-- /.row -->

@if(count(!$model->actions))
    <table class="table col-lg-12 table-striped">
        @foreach($model->actions as $key => $action)
            <tr>
                <td>{{ $action->name }}</td>
                <td>{{ $action->value }}</td>
                <td width="20">
                    {{ Form::open(array('url' => route('serviceaction.destroy', $action->id), 'class' => ' form-inline')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    <button type="submit" class="btn btn-danger visible-*-inline pull-right">
                        <span class="glyphicon glyphicon-remove"></span>
                    </button>
                    {{ Form::close() }}
                </td>
            </tr>
            <tr>
                <div class="row">

                    {{ Form::open(array('url'=>route('parameter.store'))) }}
                    {{ Form::hidden('parameterable_id', $action->id); }}
                    {{ Form::hidden('parameterable_type', 'serviceAction'); }}
                    <div class="col-lg-2">
                        <label>Name</label>
                    </div>
                    <div class="col-lg-3">
                        {{ Form::text('name', null, array('class' => 'form-control')) }}
                    </div>
                    <div class="col-lg-2">
                        <label>Value</label>
                    </div>
                    <div class="col-lg-3">
                        {{ Form::text('value', null, array('class' => 'form-control')) }}
                    </div>
                    <div class="col-lg-2">
                        {{ Form::submit('Add Parameter to action', array('class' => 'btn btn-primary pull-right')) }}
                    </div>

                    {{ Form::close() }}
                </div><!-- /.row -->
            </tr>
            @foreach($action->parameters as $parameter)
                <tr> <td>{{ $parameter->name }}</td> <td>{{ substr($parameter->value,0,20) }}</td><td>{{ Form::open(array('url' => route('parameter.destroy', $parameter->id), 'class' => 'pull-right')) }}
                        {{ Form::hidden('_method', 'DELETE') }}
                        <button type="submit" class="btn btn-danger">
                            <i class="glyphicon glyphicon-remove"></i>
                        </button>
                        {{ Form::close() }}</td></tr>
            @endforeach
        @endforeach
    </table>
    @endif


            <!-- Modal -->
    <div class="modal fade" id="createActionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            {{ Form::open(array('url'=>route('serviceaction.store'))) }}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add new action to service</h4>
                </div>
                <div class="modal-body">
                    {{ Form::label('name', 'Name') }}{{ Form::text('name', null, array('class' => 'form-control')) }}
                    {{ Form::label('description', 'Description') }}{{ Form::text('description', null, array('class' => 'form-control')) }}
                    {{ Form::label('url', 'Url') }}{{ Form::text('url', null, array('class' => 'form-control')) }}
                    {{ Form::label('request_type', 'Request type ') }}{{ Form::select('request_type', array('POST' => 'POST', 'PUT' => 'PUT', 'GET' => 'GET'), 'POST') }}
                    {{ Form::hidden('service_id', $model->id); }}
                </div>
                <div class="modal-footer">
                    {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>