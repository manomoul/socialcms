<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
{{ Form::open(array('url'=>route($modelname.'.store'))) }}
<table class="table">
    <tr><th>{{ Form::label('username', 'username') }}</th><td>{{ Form::text('username', null, array('class' => 'form-control')) }}</td></tr>
    <tr><th>{{ Form::label('email', 'email') }}</th><td>{{ Form::text('email', null, array('class' => 'form-control')) }}</td></tr>
    <tr><th>{{ Form::label('password', 'password') }}</th><td>{{ Form::password('password', array('class' => 'form-control')) }}</td></tr>
    <tr><th>{{ Form::label('password_confirmation', 'Password confirmation') }}</th><td>{{ Form::password('password_confirmation', array('class' => 'form-control')) }}</td></tr>
    {{-- Password confirmation field. -----------}}


</table>
{{ Form::submit('Save record', array('class' => 'btn btn-primary')) }}
{{ Form::close() }}