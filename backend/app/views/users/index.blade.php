<section class="row">
    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-2 pull-right">
        @include('custom_controls.btn_create', array('url'=>route($modelname.'.create')))
    </div>
</section>

<!-- Table -->
<div>
    @if(count(!$models))
        <table class="table col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <thead>
        <tr>
           <th>username</th>
           <th class="hidden-xs hidden-sm">email</th>
           <th class="hidden-xs">Last logged in</th>
            <th></th>
       </tr>
       </thead>
       <tbody>
       @foreach($models as $key => $value)
       <tr class="item">
           <td>{{{ $value->username }}}</td>
           <td class="hidden-xs hidden-sm">{{{ $value->email }}}</td>
           <td class="hidden-xs">{{{ $value->lastlogin_at }}}</td>
           <td>@include('custom_controls.btn_delete', array('url'=>route($modelname.'.destroy', $value->id)))</td>
       </tr>
    @endforeach
    </tbody>
    </table>
    @else

   

    <div class="panel panel-info">
      <div class="panel-body">
        There are no records
      </div>
    </div>
    @endif
</div>
<div class="row">
    <div class="pull-right">
        {{ $models->links() }}
    </div>
</div>

