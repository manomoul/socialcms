var gulp   = require('gulp'),
	bower  = require('gulp-bower'),
	notify = require('gulp-notify'),
	rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
	sass   = require('gulp-ruby-sass'),
    watch = require('gulp-watch');

// Config
// ------
	paths = {
		bower: "./bower_components/",
        dist: "./public/",
        src: "./src/"
	};

// Tasks
// -----
/**
 * Run all tasks when 'gulp' is executed.
 */
gulp.task('default', ['css', 'js', 'watch'], function() {
    // Default task must exist.
});

/**
 * Install Bower packages defined in bower.json into the given components folder.
 */
gulp.task('bower', function() {
	return bower()
		.pipe(gulp.dest(paths.bower));
});

/**
 * Create font files in destination folder.
 */
gulp.task('fonts', ['bower'], function() {
    gulp.src(paths.bower + 'fontawesome/f**/*.*')
        .pipe(gulp.dest(paths.dist));

    gulp.src(paths.bower + 'bootstrap-sass-official/vendor/assets/f**/**/*.*')
        .pipe(gulp.dest(paths.dist));
});

/**
 * Create css files in destination folder.
 */

gulp.task('css', ['bower', 'fonts'], function() {
    var configSass = {
        style: 'compressed',
        loadPath: [
                paths.src   + 'css',
                paths.bower + 'bootstrap-sass-official/vendor/assets/stylesheets/bootstrap',
                paths.bower + 'fontawesome/scss'
            ] 
        };

    gulp.src(paths.bower+'bootstrap-sass-official/vendor/assets/stylesheets/*.scss')
        .pipe(sass(configSass))
        .pipe(rename({ suffix: '.min' }))
        .on('error', function (err) { console.log(err.message); })
        .pipe(gulp.dest('./public/css'));
    
    gulp.src(paths.src + 'css/*.scss')
        .pipe(sass(configSass))
        .pipe(rename({ suffix: '.min' }))
        .on('error', function (err) { console.log(err.message); })
        .pipe(gulp.dest('./public/css'));
    
});

/**
 * Create JS files in destination folder.
 */
gulp.task('js', ['bower'], function() {

    // Bootstrap JS
    gulp.src(paths.bower + 'bootstrap-sass-official/assets/javascripts/bootstrap.js')
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(paths.dist + 'js'));

    gulp.src(paths.src + 'js/*.js', { base: paths.src })
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(paths.dist))
});

/* watch css*/
gulp.task('watch', ['js'], function(){
    gulp.watch([
                paths.bower+'bootstrap-sass-official/vendor/assets/stylesheets/*.scss',
                paths.src + 'css/*.scss',
                ], ['comp_css']);
  
});

gulp.task('comp_css', function() {
    var conf = {
        style: 'compressed',
        loadPath: [
                paths.src   + 'css',
                paths.bower + 'bootstrap-sass-official/vendor/assets/stylesheets/bootstrap',

            ] 
        };

    gulp.src(paths.bower+'bootstrap-sass-official/vendor/assets/stylesheets/*.scss')
        .pipe(sass(conf))
        .pipe(rename({ suffix: '.min' }))
        .on('error', function (err) { console.log(err.message); })
        .pipe(gulp.dest('./public/css'));
    
    gulp.src(paths.src + 'css/*.scss')
        .pipe(sass(conf))
        .pipe(rename({ suffix: '.min' }))
        .on('error', function (err) { console.log(err.message); })
        .pipe(gulp.dest('./public/css'));
    
});

