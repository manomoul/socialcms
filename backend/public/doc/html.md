#HTML 
## Wireframes

###1.0 Blogposts

![Blogposts](https://lh4.googleusercontent.com/KGEOtZYN91IAthYO2PEkGtE-v4Dl-sQLPRS9lHGikrr_HuKWAsdG9RLGw4AjNI0wBf2apdN6xiA=w1408-h594 "Title here")

###1.1 Blogpost_create

![Blogpost_create](https://lh5.googleusercontent.com/YAufH_jW8nlEaigeV4HrKobV-JvtPXvYBDWay6U_ruJwDbeg05l7GVclzPALZCbGDCN-y-t9lbo=w1408-h594 "Title here")

###2.0 Events
![Events](https://lh6.googleusercontent.com/fZ8Rx6Fi_eRmjBKhBDC_erBHReEobffRbEIDRR3xHo_4GjsUxkKrP-oIs4uoQLsSWUqYv3JL27g=w1408-h594
 "Title here")

###2.1 Event_create
![Events](https://lh4.googleusercontent.com/FpjGl7FGL_nU-Ss8G31pDXgLj-nisN8hy8pAYwrAQw-YH2WYdvxK6DrCeZOVqlL5qoT5U5wa-wM=w1408-h594
 "Title here") 
 
###3.0 Services
![Service view](https://lh3.googleusercontent.com/S4ZvodQvArGaUpBGFb5CrWYBSd29znrNtV32VmxJILaTet3875aYX9DyyDlt-PWdIy6YG94bOhw=w1408-h594
 "Title here")
 
###3.1 Service_View
![Service view](https://lh3.googleusercontent.com/cbmK9jfAIghp12WMafHMasxCjtfeSejTwQE6Ydy3gGCTgt0QRKcH1nNFyc1SyeqcuCXHeCjUJ4Y=w1408-h594
 "Title here")

###4.0 Configuration
![Configuration](https://lh6.googleusercontent.com/SjDzWZu8HetcQ7e_LiaBLh_eMvC6ryA8DLYsST2UhRhZJ-iGPATHq9TJdT-1MBIVa7PCPC2cbLE=w1408-h594 "Title here")



 


