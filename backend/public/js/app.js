/**
 * Created by manonmoulin on 22/12/14.
 */

(function() {
    var app = angular.module('nmbm', ['ui.bootstrap', 'slugifier']);

    app.config(function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });


    app.controller('TabController', function(){
        this.tab = 0;

        this.setTab = function(selectedTab){
            this.tab = selectedTab;
        };

        this.isSet = function(givenTab){
            return this.tab === givenTab;
        };
    });

    app.controller('ImageDragController', function(){
        this.allowDrop = function(event) {
            console.log(ev);
            event.preventDefault();
        };

        this.drop = function(ev) {
            console.log(ev);
            ev.preventDefault();
            var data = ev.dataTransfer.getData("text");
            ev.target.appendChild(document.getElementById(data));
        }

    });


})();

//

