'use strict';

/* App Module */

var nmbmApp = angular.module('nmbmApp', [
    'ngRoute',
  //  'nmbmAnimations',
    'nmbmControllers',
    'nmbmFilters',
    'nmbmServices',
    'ngSanitize'
]);

nmbmApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
           /* when('/models/events', {
                templateUrl: 'parts/event-list.html',
                controller: 'EventListCtrl'
            }).
            when('/models/events/:eventId', {
                templateUrl: 'parts/event-detail.html',
                controller: 'EventDetailCtrl'
            }).
            when('/', {
                templateUrl: 'parts/page-list.html',
                controller: 'PageListCtrl'
            }).*/
            when('/:pageSlug', {
                templateUrl: 'parts/page-detail.html',
                controller: 'PageDetailCtrl'
            }).
            otherwise({
            redirectTo: 'home'
            })
        /*.
            when('/', {
                templateUrl: 'parts/page-detail.html',
                controller: 'PageDetailCtrl'
            }) .
           otherwise({
                redirectTo: '/events'
            })*/;
    }]);