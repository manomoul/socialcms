'use strict';

/* Controllers */

var nmbmControllers = angular.module('nmbmControllers', []);


/* *
* Main Controller
* */
nmbmControllers.controller('MainCtrl', ['$scope', 'Page', 'Company',
    function($scope, Page, Company) {
        $scope.pages = Page.query();
        $scope.orderProp = 'weight';
        $scope.date = new Date();
        $scope.backenUrl = "http://backend.notmadebymanon.dev/"
        $scope.company = Company.query();
       // $scope.orderProp = 'weight';
    }]);
/* *
* Page Controller
* */
nmbmControllers.controller('PageDetailCtrl', ['$scope', '$routeParams', 'Page', function($scope, $routeParams, Page) {
    $scope.page = Page.get({pageSlug: $routeParams.pageSlug}, function(page) {

    });
}]);
nmbmControllers.controller('PageListCtrl', ['$scope', 'Page',
    function($scope, Page) {
        $scope.pages = Page.query();
        $scope.orderProp = 'weight';
    }]);
/* *
 * Event Controllers
 * */
nmbmControllers.controller('EventListCtrl', ['$scope', 'Event',
    function($scope, Event) {
        $scope.events = Event.query();
        $scope.orderProp = 'published_date';
    }]);

nmbmControllers.controller('EventDetailCtrl', ['$scope', '$routeParams', 'Event',
    function($scope, $routeParams, Event) {
        $scope.event = Event.get({eventId: $routeParams.eventId}, function(event) {
        });
    }]);
/* *
 * Contact Controller
 * */
nmbmControllers.controller('ContactCtrl', ['$scope', '$routeParams', 'Page', function($scope, $routeParams, Page) {
    $scope.page = Page.get({pageSlug: $routeParams.pageSlug}, function(page) {

    });
}]);