'use strict';

/* Services */

var nmbmServices = angular.module('nmbmServices', ['ngResource']);
var backendUrl = "http://backend.notmadebymanon.dev";
/**
 * Event
 * **/
nmbmServices.factory('Event', ['$resource',
    function($resource){
        return $resource(backendUrl+'/api/v1/events/:eventId', {}, {
            query: {method:'GET', params:{eventId: ''}, isArray:true}
        });
    }]);
/**
 * Page
 * **/
nmbmServices.factory('Page', ['$resource',
    function($resource){
        return $resource(backendUrl+'/api/v1/pages/:pageSlug', {}, {
            query: {method:'GET', params:{pageSlug:''}, isArray:true}
        });
    }]);
/**
 * Company
 * **/
nmbmServices.factory('Company', ['$resource',
    function($resource){
        //console.log($resource('http://backend.notmadebymanon.dev/api/v1/pages/:pageSlug'));
        return  $resource(backendUrl+'/api/v1/config', {}, {
            query: {method:'GET', isArray:false}
        });
    }]);
/**
 * Contact
 * */
nmbmServices.factory('Contact', ['$resource',
    function($resource){
        //console.log($resource('http://backend.notmadebymanon.dev/api/v1/pages/:pageSlug'));
        return  $resource(backendUrl+'/api/v1/sendEmail', {}, {
            send: {method:'POST', params:{charge:true}}
        });
    }]);
